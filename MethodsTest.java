public class MethodsTest{
	public static void main(String[] args){
		int x = 5;
		System.out.println("Value before the methodNoInputNoReturn: "+x);
		methodNoInputNoReturn();
		System.out.println("Value after the methodNoInputNoReturn: "+x);
	//------------------------------------------------------------------------
		System.out.println("Value before the methodOneInputNoReturn: "+x);
		methodOneInputNoReturn(x+10);	
		System.out.println("Value after the methodOneInputNoReturn: "+x);
	//-------------------------------------------------------------------------
		methodTwoInputNoReturn(1,2);
	//--------------------------------------------------------------------------
	    int y = methodNoInputReturnInt();
		System.out.println(y);
	//--------------------------------------------------------------------------
	    double z = sumSquareRoot(9,5);
		System.out.println(z);
	//--------------------------------------------------------------------------
	    String s1= "java";
		String s2= "Programming";
		System.out.println("Length of s1: "+s1.length());
		System.out.println("Length od s2: "+s2.length());
	//--------------------------------------------------------------------------
	    System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	//--------------------------------------------------------------------------
		
	}
	public static void methodNoInputNoReturn(){
		int x = 20;
		System.out.println("I’m in a method that takes no input and returns nothing");
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int y){
		y=y-5;
		System.out.println("Inside the method one input no return");
		System.out.println(y);
	}
	public static void methodTwoInputNoReturn(int x, double y){
	    System.out.println("Value 1: "+x);
		System.out.println("Value 2: "+y);
	}
	public static int methodNoInputReturnInt(){
	return 5;
	}
	public static double sumSquareRoot(int x, int y){
		int z = x+y;
		return Math.sqrt(z);
	}
}